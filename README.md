# Nixvim Dev-shell Environment

This is a Nix flake configuration for Neovim built with the [Nixvim project](https://github.com/nix-community/nixvim)
It is fully configured with LSPs, linters, formatters, debuggers, styling, and
popular navigation plugins.

## Setup

This flake can be built with the Nix package manager or with a NixOS system
configured with flake support. [Follow these instructions](https://nixos.org/download#download-nix)
to download and install the Nix package manager for your system.

Once you have the package manager installed, enable flak support by adding
the following line to ~/.config/nix/nix.conf or /etc/nix/nix.conf:

```
experimental-features = nix-command flakes
```

You will need to restart your shell or terminal session for the setting to take effect.

```
nix develop
```

or run it directly from Github with:

```
nix develop gitlab:daynyxx/nixvim-flake
```

You can create a persistent profile link for the development environment with:

```
nix develop --profile ./nixvim
```

"nixvim" is an arbitrary name. Use whatever name you like. This will create a
symlink to the Nix store environment derivation. You can re-open the profile with:

```
nix develop ./nixvim-flake
```

## Styling

- Theme is [one](https://github.com/Th3Whit3Wolf/one-nvim) with
  [noice](https://github.com/folke/noice.nvim) UI from [@folke](https://github.com/folke)
- [lualine](https://github.com/nvim-lualine/lualine.nvim) with onedark theme

## Language Support

---

| Language   | LSP | Lint | Format | Debug |
| ---------- | :-: | :--: | :----: | :---: |
| Ansible    | ✔   |   ✔  |   ✔    |  ✔\*  |
| Lua        | ✔   |  ✔   |   ✔    |       |
| Markdown   | ✔   |  ✔   |   ✔    |       |
| Nix        | ✔   |  ✔   |   ✔    |       |
| Python     | ✔   |  ✔   |   ✔    | untested |
| Shell      | ✔   |  ✔   |   ✔    | ✔\*   |
| TypeScript | ✔   |  ✔   |   ✔    |       |
| YAML       | ✔   |  ✔   |   ✔    |       |

---

## Plugins

- [cmp-buffer](https://github.com/hrsh7th/cmp-buffer) - nvim-cmp source for buffer words
- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline) - nvim-cmp source for cmdline
- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp) - nvim-cmp source for LSP client
- [cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua) - nvim-cmp source for lua
- [cmp-path](https://github.com/hrsh7th/cmp-path) - nvim-cmp source for path
- [conform](https://github.com/stevearc/conform.nvim) - Formatter plugin
- [fugitive](https://github.com/tpope/vim-fugitive) - Git wrapper
- [gitsigns](https://github.com/lewis6991/gitsigns.nvim) - Git integration for buffers
- [harpoon](https://github.com/ThePrimeagen/harpoon) - Fast file/buffer switching
- [lualine](https://github.com/nvim-lualine/lualine.nvim) - Statusline
- [luasnip](https://github.com/L3MON4D3/LuaSnip) - Snippet engine
- [noice](https://github.com/folke/noice.nvim) - Experimental messaging, cmdline, and popupmenu UI
- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp) - Code completion
- [nvim-lint](https://github.com/mfussenegger/nvim-lint) - Async linter plugin
- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) - Language Server Protocol config tool
- [nvim-notify](https://github.com/rcarriga/nvim-notify) - Notification manager
- [nvim-ufo](https://github.com/kevinhwang91/nvim-ufo) - Ultra Fold in Neovim
- [oil](https://github.com/stevearc/oil.nvim) - File explorer
- [telescope](https://github.com/nvim-telescope/telescope.nvim) - Fuzzyfinder
- [treesitter-textobjects](https://github.com/nvim-treesitter/nvim-treesitter-textobjects) - Syntax aware text-objects
- [treesitter](https://github.com/nvim-treesitter/nvim-treesitter?tab=readme-ov-file) - Parsing system interface
- [undotree](https://github.com/mbbill/undotree) - Undo history visualizer

## Resources

Much of the configuration for this flake was gleaned from numerous instructional
videos and howto guides for Neovim including:

- Keymaps and Harpoon configuration from [ThePrimeagen's video](https://www.youtube.com/watch?v=w7i4amO_zaE)
- LSP setup guide with [TJ DeVries and Bashbunni](https://youtu.be/puWgHa7k3SY?list=PL3PYGQRVAjrMxP5HK45CTnR7Yv-QYR1Qp)
- Linting and Formatting from [Josean Martinez](https://youtu.be/ybUE4D80XSk)
- Telescope and Treesitter setup again [with TJ DeVries](https://youtu.be/stqUbv-5u2s)
- In-depth [Treesitter configuration with Josean Martinez](https://www.youtube.com/watch?v=CEMPq_r8UYQ&list=LL&index=1&t=40s)
- Completions with nvim-cmp [from TJ again](https://youtu.be/_DnmphIwnjo)
- Debugging with DAP with [TJ and Bashbunni](https://youtu.be/0moS8UHupGc)

## Thanks

Big shout out to the Nix and Neovim community for all the awesome plugins, documentation,
videos, and support. There are too many people to thank, but for this project in
particular, I'd like to thank [@vimjoyer](https://github.com/vimjoyer) for piquing
my interest in Nixvim with [his video](https://youtu.be/b641h63lqy0) and [@GaetanLepage](https://github.com/GaetanLepage)
for maintaining the Nixvim project. This project is forked from [pete3n's flake](https://github.com/pete3n/nixvim-flake) 
and changed to fit with my neovim config
