{pkgs, ...}: {
  keymaps = [
    {
      key = "jk";
      mode = "i";
      action = "<ESC>";
    }
    {
      key = "<leader>pv";
      mode = "n";
      action = "<cmd>Oil<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[p]roject [v]iew";
      };
    }
    {
      key = "<leader>u";
      mode = "n";
      action = "<cmd>UndotreeToggle<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[u]ndotree toggle";
      };
    }
    {
      key = "<leader>gs";
      mode = "n";
      action = "<cmd>Git<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[g]it [s]tatus";
      };
    }
    {
      key = "<leader>gc";
      mode = "n";
      action = "<cmd>Git commit<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[g]it [c]ommit";
      };
    }
    {
      key = "<leader>nh";
      mode = "n";
      action = "<cmd>noh<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[n]o [h]ilight";
      };
    }
    {
      key = "<C-y>";
      mode = "n";
      action = ":set cursorcolumn!<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "Toggle vertical column because [Y]AML sucks";
      };
    }
    {
      key = "J";
      mode = "v";
      action = ":m '>+1<CR>gv=gv";
      options = {
        silent = true;
        noremap = true;
        desc = "Shift line down 1 in visual mode";
      };
    }
    {
      key = "K";
      mode = "v";
      action = ":m '<-2<CR>gv=gv";
      options = {
        silent = true;
        noremap = true;
        desc = "Shift line up 1 in visual mode";
      };
    }
    {
      key = "J";
      mode = "n";
      action = "mzJ\`z"; # Keep cursor to the left
      options = {
        silent = true;
        noremap = true;
      };
    }
    {
      key = "<C-d>";
      mode = "n";
      action = "<C-d>zz"; # Keep cursor in middle
      options = {
        silent = true;
        noremap = true;
      };
    }
    {
      key = "<C-u>";
      mode = "n";
      action = "<C-u>zz"; # Keep cursor in middle
      options = {
        silent = true;
        noremap = true;
      };
    }
    {
      key = "n";
      mode = "n";
      action = "nzzzv"; # Keep cursor in middle
      options = {
        silent = true;
        noremap = true;
      };
    }
    {
      key = "N";
      mode = "n";
      action = "Nzzzv";
      options = {
        silent = true;
        noremap = true;
      };
    }
    {
      key = "<leader>p";
      mode = "x";
      action = "\"_dP";
      options = {
        silent = true;
        noremap = true;
        desc = "[p]reserve put";
      };
    }
    {
      key = "<leader>y";
      mode = ["n" "v"];
      action = "\"+y";
      options = {
        silent = true;
        noremap = true;
        desc = "[y]ank to system clipboard";
      };
    }
    {
      key = "<leader>Y";
      mode = "n";
      action = "\"+Y";
      options = {
        silent = true;
        noremap = true;
        desc = "[Y]ank line to system clipboard";
      };
    }
    {
      key = "Q";
      mode = "n";
      action = "<nop>";
      options = {
        silent = true;
        noremap = true;
        desc = "Don't";
      };
    }
    {
      key = "<C-f>";
      mode = "n";
      action = "<cmd>!tmux neww tmux-sessionizer<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[f]ind and switch tmux session";
      };
    }
    {
      key = "<leader>k";
      mode = "n";
      action = "<cmd>lnext<CR>zz";
      options = {
        silent = true;
        noremap = true;
        desc = "Next quickfix location";
      };
    }
    {
      key = "<leader>j";
      mode = "n";
      action = "<cmd>lprev<CR>zz";
      options = {
        silent = true;
        noremap = true;
        desc = "Prev quickfix location";
      };
    }
    {
      key = "<leader>mp";
      mode = ["n" "v"];
      action = ":lua _G.format_with_conform()<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[m]ake [p]retty by formatting";
      };
    }
    {
      key = "<leader>da";
      mode = "n";
      action = ":lua vim.lsp.buf.code_action()<CR>";
      options = {
        silent = true;
        noremap = true;
        desc = "[d]iagnostic changes [a]ccepted";
      };
    }
  ];
}
