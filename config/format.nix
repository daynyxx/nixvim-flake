{pkgs, ...}: {
  plugins.conform-nvim = {
    enable = true;
    settings = {
      formatters_by_ft = {
        asm = ["asmfmt"];
        c = ["astyle"];
        cpp = ["astyle"];
        css = ["prettierd"];
        cmake = ["cmake_format"];
        go = ["gofumpt"];
        html = ["prettierd"];
        javascript = ["prettierd"];
        javascriptreact = ["prettier"];
        json = ["prettier"];
        lua = ["stylua"];
        markdown = ["prettier"];
        nix = ["alejandra"];
        python = ["isort" "black"];
        rust = ["rustfmt"];
        sh = ["shfmt"];
        typescript = ["prettierd"];
        typescriptreact = ["prettier"];
        # yaml = ["yamlfmt"];
      };
      formatters = {
        asmfmt = {
          command = "asmfmt";
          stdin = true;
        };
      };
      format_on_save = {
        lspFallback = true;
        timeoutMs = 2000;
      };
    };
  };

  extraConfigLuaPre = ''
    -- Formatting function for conform
    _G.format_with_conform = function()
    	local conform = require("conform")
    	conform.format({
    		lsp_fallback = true,
    		async = false,
    		timeout_ms = 2000,
    	})
    end
  '';

  extraPackages = [
    pkgs.alejandra
    pkgs.asmfmt
    pkgs.astyle
    pkgs.black
    pkgs.cmake-format
    pkgs.gofumpt
    pkgs.prettierd
    pkgs.rustfmt
    pkgs.shfmt
  ];
}
