{pkgs, ...}: {
  plugins.telescope = {
    enable = true;
    keymaps = {
      "<leader>?" = {
        action = "oldfiles";
        options = {
          desc = "[?] Find recently opened files";
        };
      };
      "<leader>/" = {
        action = "current_buffer_fuzzy_find";
        options = {
          desc = "[/] Fuzzily search in current buffer]";
        };
      };
      "<C-p>" = {
        action = "find_files";
        options = {
          desc = "search files";
        };
      };
      "<C-g>" = {
        action = "git_branches";
        options = {
          desc = "search branches";
        };
      };
      "<leader>sh" = {
        action = "help_tags";
        options = {
          desc = "[s]earch [h]elp";
        };
      };
      "<leader>sp" = {
        action = "grep_string";
        options = {
          desc = "[s]earch current [p]roject";
        };
      };
      "<leader>sg" = {
        action = "live_grep";
        options = {
          desc = "[s]earch by [g]rep";
        };
      };
      "<leader>sd" = {
        action = "diagnostics";
        options = {
          desc = "[s]earch [d]iagnotics";
        };
      };
      "<leader>sk" = {
        action = "keymaps";
        options = {
          desc = "[s]earch [k]eymaps";
        };
      };
      "<leader>ff" = {
        action = "lsp_document_symbols symbols='function'";
        options = {
          desc = "[f]ind [f]unction";
        };
      };
    };
    settings = {
      defaults = {
        file_ignore_patterns = [
          "^.git/"
          "^.mypy_cache/"
          "^__pycache__/"
          "^output/"
          "^data/"
          "%.ipynb"
          "node%_modules/.*"
        ];
      };
    };
  };
  extraPackages = [
    pkgs.ripgrep
    pkgs.fd
  ];
}
