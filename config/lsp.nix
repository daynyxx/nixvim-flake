{...}: {
  plugins.lsp = {
    enable = true;
    servers = {
      ansiblels.enable = true;
      bashls.enable = true;
      dartls.enable = true;
      lua_ls.enable = true;
      gopls.enable = true;
      nixd.enable = true;
      ruff.enable = true;
      rust_analyzer = {
        enable = true;
        installCargo = true;
        installRustc = true;
      };
      yamlls.enable = true;
    };
  };
  extraConfigLua = ''
    -- Common LSP key mappings
    local function set_cmn_lsp_keybinds()
      local lsp_keybinds = {
        {
          key = "K",
          action = vim.lsp.buf.hover,
          options = {
            buffer = 0,
            desc = "hover [K]noledge with LSP",
          },
        },
        {
          key = "gd",
          action = vim.lsp.buf.definition,
          options = {
            buffer = 0,
            desc = "[g]o to [d]efinition with LSP",
          },
        },
        {
          key = "gy",
          action = vim.lsp.buf.type_definition,
          options = {
            buffer = 0,
            desc = "[g]o to t[y]pe definition with LSP",
          },
        },
        {
          key = "gi",
          action = vim.lsp.buf.implementation,
          options = {
            buffer = 0,
            desc = "[g]o to [i]mplementation with LSP",
          },
        },
        {
          key = "<leader>dj",
          action = vim.diagnostic.goto_next,
          options = {
            buffer = 0,
            desc = "Go to next [d]iagnostic with LSP",
          },
        },
        {
          key = "<leader>dk",
          action = vim.diagnostic.goto_prev,
          options = {
            buffer = 0,
            desc = "Go to previous [d]iagnostic with LSP",
          },
        },
        {
          key = "<leader>ds",
          action = vim.diagnostic.open_float,
          options = {
            buffer = 0,
            desc = "Show diagnostic in popup window"
          },
        },
        {
          key = "<leader>rnv",
          action = vim.lsp.buf.rename,
          options = {
            buffer = 0,
            desc = "[r]e[n]ame [v]ariable with LSP",
          },
        },
      }

      for _, bind in ipairs(lsp_keybinds) do
        vim.keymap.set("n", bind.key, bind.action, bind.options)
      end
    end

    -- Additional lsp-config
    local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())
    capabilities.textDocument.completion.completionItem.snippetSupport = true

    vim.api.nvim_create_autocmd(
      {
        "BufEnter",
        "BufRead",
        "BufNewFile",
      },
      {
        pattern = "**/roles/*.yaml,**/roles/**/*.yaml,**/roles/**/*.yml",
        callback = function()
          local buf = vim.api.nvim_get_current_buf()
          vim.api.nvim_buf_set_option(buf, "filetype", "yaml.ansible")
        end
      }
    )

    -- Individual LSP configs
    -- Ansible LSP
    require("lspconfig").ansiblels.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Bash LSP
    require("lspconfig").bashls.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Dart LSP
    require("lspconfig").dartls.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Go LSP
    require("lspconfig").gopls.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Lua LSP
    require("lspconfig").lua_ls.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Markdown LSP
    require("lspconfig").marksman.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Nix LSP
    require("lspconfig").nixd.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- Python LSP
    require("lspconfig").ruff.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    require("lspconfig").rust_analyzer.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })

    -- YAML LSP
    require("lspconfig").yamlls.setup({
      on_attach = function()
        set_cmn_lsp_keybinds()
      end,
    })
  '';
}
