{pkgs, ...}: {
  plugins.harpoon= {
    enable = true; # The name, is the harpoon-maker-agen
    enableTelescope = true;
    keymaps = {
      addFile = "<leader>a";
      toggleQuickMenu = "<C-e>";
      navFile = {
        "1" = "<C-h>";
        "2" = "<C-j>";
        "3" = "<C-k>";
        "4" = "<C-l>";
      };
    };
  };
}
