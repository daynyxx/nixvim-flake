{...}: {
  plugins = {
    neorg = {
      enable = true;
      settings = {
        "core.dirman" = {
          config = {
            workspaces = {
              work = "~/notes/work";
            };
          };
        };
      };
    };
  };
}
