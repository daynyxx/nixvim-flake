{pkgs, ...}: {
  imports = [
    ./completion.nix
    ./format.nix
    ./harpoon.nix
    ./keymaps.nix
    ./lint.nix
    ./lsp.nix
    ./style.nix
    ./telescope.nix
    ./treesitter.nix
    #    ./debug.nix
  ];

  config = {
    editorconfig.enable = true;

    autoCmd = [
      {
        command = "setfiletype yaml.ansible";
        event = [
          "BufEnter"
          "BufWinEnter"
        ];
        pattern = [
          "*.yaml"
        ];
      }
    ];

    globals = {
      mapleader = " ";
    };

    opts = {
      autoindent = true;
      backup = false; #Undotree
      colorcolumn = "120";
      cursorline = true;
      encoding = "UTF-8";
      expandtab = true;
      foldenable = true;
      foldlevelstart = 15;
      foldmarker = "{,}";
      foldmethod = "indent";
      history = 100;
      hlsearch = true;
      ignorecase = true;
      incsearch = true;
      laststatus = 2;
      nu = true;
      number = true;
      relativenumber = true;
      rnu = true;
      ruler = true;
      scrolloff = 8;
      shiftwidth = 2;
      showmatch = true;
      sidescroll = 2;
      signcolumn = "yes";
      smartcase = true;
      swapfile = false; #Undotree
      tabstop = 2;
      termguicolors = true;
      undofile = true;
      updatetime = 50;
      wildmenu = true;
      wildmode = "longest:full,full";
      wrap = false;
    };

    plugins = {
      gitsigns.enable = true;
      oil.enable = true;
      toggleterm.enable = true;
      undotree.enable = true;
      fugitive.enable = true;
      neorg.enable = true;
    };
    extraPackages = with pkgs; [
      # Formatters
      stylua
      # Language Servers
      marksman
      # Compiler / Language
      ansible
      gcc
      glibc
      go
      isort
      lua
      nodejs_20
      # Misc Dependencies
      luajitPackages.jsregexp
      luajitPackages.lua-utils-nvim
      nmap
      nodePackages.prettier
      python3
    ];
  };
}
